import { PokeApi } from './Constants'; 

export const pokeServices = {
    getFirstPokes
}

function getFirstPokes(){
    const requestOptions = {
        method: 'GET',
    };

    return fetch(PokeApi.GETFIRSTPOKES, requestOptions).then(handleResponse);
    
}