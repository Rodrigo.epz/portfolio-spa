import axios from "axios";
import React, { Fragment } from "react";
import { Badge } from "reactstrap";
import { PokeApi } from "../../services/Constants";
import { servicesFunctions } from "../../utils/serviceHandlers";

class Home extends React.Component {
  state = {
    title: "Rodrigo´s Portfolio",
    pokemons: [],
    pokemonsImages: [],
  };

  componentDidMount() {
    axios.get(PokeApi.GETFIRSTPOKES).then(servicesFunctions.handleAxiosResponse).then((res) => {

      console.log(res)
      this.setState({pokemons: res})
      
      servicesFunctions.getImages(res).then(images => {
        console.log(images)
        this.setState({pokemonsImages : images})
      });
      
    
    });  

  }

  

  render() {
    return (
      <Fragment>
        <h1>
          {this.state.title} <Badge color="secondary">1.0</Badge>
        </h1>
        <ul>
          {this.state.pokemons && this.state.pokemons.length  && this.state.pokemonsImages && this.state.pokemonsImages.length && this.state.pokemons.map((pokemon, i) => (
            <li>{pokemon.name} <img src={this.state.pokemonsImages[i]} alt={pokemon.name}></img></li>
          ))}
        </ul>
      </Fragment>
    );
  }
}

export default Home;
