import "./App.css";
import { BrowserRouter as Router } from "react-router-dom";
import RouterConfig from "./navigation/RouterConfig";
import { Fragment } from "react";
import { Alert } from "reactstrap";

function App(props) {

  const { alert } = props;
  return (
    <Fragment>
      {alert && alert.message && (
        <Alert
          fade={true}
          toggle={this.onDismiss}
          className={`${alert.type} right-alert`}
        >
          {alert.message}
        </Alert>
      )}
      <Router>
        <RouterConfig></RouterConfig>
      </Router>
    </Fragment>
  );
}

export default App;
