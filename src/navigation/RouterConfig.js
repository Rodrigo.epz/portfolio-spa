import { Route, Switch } from "react-router-dom";
import Home from "../pages/home";
import { navConstants } from "./Constants";

export const RouterConfig = () => {
    return (
        <Switch>
            <Route exact path={navConstants.URLHOME} component={Home}></Route>
        </Switch>
    );
};

export default RouterConfig;