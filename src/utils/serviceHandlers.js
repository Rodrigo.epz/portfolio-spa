import axios from "axios";


export const servicesFunctions = {
    handleAxiosResponse,
    getImages
}

function handleAxiosResponse(response) {
    console.log(response)
    return response.data.results
}

async function getImages (res) { 
    try {
        let images = [];
        console.log(res)

        for (const poke of res) {
            console.log(poke)

            let resp = await axios(
                {
                    url: poke.url,
                    method: 'get',
                    timeout: 8000,
                    headers: {
                        'Content-Type': 'application/json',
                    }
                })
            images.push(resp.data.sprites.front_default)
        }
      
        
        return images
    } catch (err) {
        // Handle Error Here
        console.error(err);
    }
   
}